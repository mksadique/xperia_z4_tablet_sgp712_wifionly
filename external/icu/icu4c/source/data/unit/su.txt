﻿// ***************************************************************************
// *
// * Copyright (C) 2015 International Business Machines Corporation
// * Copyright (C) 2015 Sony Mobile Communications Inc.
// * and others. All Rights Reserved.
// * Tool: org.unicode.cldr.icu.NewLdml2IcuConverter
// *
// ***************************************************************************
su{
    Version{"2.1.12.94"}
    units{
        day{
            other{"{0} poe"}
        }
        day-future{
            one{"{0} poe"}
            other{"{0} poe"}
        }
        day-past{
            other{"{0} poe katukang"}
        }
        hour{
            other{"{0} jam"}
        }
        hour-future{
            other{"Dina {0} jam"}
        }
        hour-past{
            other{"{0} jam katukang"}
        }
        minute{
            other{"{0} menit"}
        }
        minute-future{
            other{"Dina {0} menit"}
        }
        minute-past{
            other{"{0} menit katukang"}
        }
        month{
            other{"{0} sasih"}
        }
        month-future{
            other{"Dina {0} sasih"}
        }
        month-past{
            other{"{0} sasih katukang"}
        }
        second{
            other{"{0} detik"}
        }
        second-future{
            other{"Dina {0} detik"}
        }
        second-past{
            other{"{0} detik katukang"}
        }
        week{
            other{"{0} minggu"}
        }
        week-future{
            other{"Dina {0} minggu"}
        }
        week-past{
            other{"{0} minggu katukang"}
        }
        year{
            other{"{0} taun"}
        }
        year-future{
            other{"Dina {0} taun"}
        }
        year-past{
            other{"{0} taun katukang"}
        }
    }
    unitsShort{
        day{
            other{"{0} hr"}
        }
        hour{
            other{"{0} jam"}
        }
        minute{
            other{"{0} mnt"}
        }
        month{
            other{"{0} bln"}
        }
        second{
            other{"{0} dtk"}
        }
        week{
            other{"{0} mggu"}
        }
        year{
            other{"{0} thn"}
        }
    }
    }
