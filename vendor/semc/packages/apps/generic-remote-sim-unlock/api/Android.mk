# Copyright (C) 2014 Sony Mobile Communications AB.
# All rights, including trade secret rights, reserved.

LOCAL_PATH := $(call my-dir)
TMP_LOCAL_PATH := $(LOCAL_PATH)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := tests

LOCAL_PACKAGE_NAME := GenericOTARemoteUnlockTest

LOCAL_SRC_FILES := $(call all-java-files-under, src)

include $(BUILD_PACKAGE)

LOCAL_PATH := $(TMP_LOCAL_PATH)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_MODULE := com.sonymobile.simlock.communication

LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_JAVA_LIBRARY)

LOCAL_PATH := $(TMP_LOCAL_PATH)
include $(CLEAR_VARS)

LOCAL_MODULE := com.sonymobile.simlock.communication.xml

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_CLASS := ETC

LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions

LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)
