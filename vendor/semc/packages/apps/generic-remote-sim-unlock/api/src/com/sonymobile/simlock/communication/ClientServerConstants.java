/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication;

public interface ClientServerConstants {
    /**
     * Client message, version 1.
     */
    byte SONY_REMOTE_UNLOCK_CLIENT_DATA_V1 = 0x10;

    /**
     * Server message, version 1.
     */
    byte SONY_REMOTE_UNLOCK_SERVER_DATA_V1 = 0x11;

    /**
     * Constant used in RemoteUnlockRequest to request a permanent unlock.
     */
    byte SONY_REMOTE_UNLOCK_PERMANENT      = 0x20;

    /**
     * Constant used in RemoteUnlockRequest to request a temporary unlock.
     */
    byte SONY_REMOTE_UNLOCK_TEMPORARY      = 0x21;

    /**
     * Constant used in RemoteUnlockRequest to request a reconfiguration of
     * the simlock. A reconfiguration means that a new simlock configuration
     * is fetched from the server, possibly enabling other networks.
     */
    byte SONY_REMOTE_UNLOCK_RECONFIG       = 0x22;

    /**
     * Length of IMEI, zero padded at the end.
     */
    int  IMEI_LENGTH                       = 16;

    /**
     * Max length of the simlock blob.
     */
    int  MAX_BLOB_LENGTH                   = (2048 + 1024 + 4);

    /**
     * Return value from getSimlockStatus()
     * Simlock state unlocked.
     */
    int SIMLOCK_STATE_UNLOCKED             = 0;

    /**
     * Return value from getSimlockStatus()
     * Simlock state locked.
     */
    int SIMLOCK_STATE_LOCKED               = 1;

    /**
     * Return value from getSimlockStatus()
     * Simlock state temporary unlocked.
     */
    int SIMLOCK_STATE_TEMP_UNLOCKED        = 2;

    /**
     * Return value from getSimlockStatus()
     * Simlock state unlocked.
     * Error occurred while getting the simlock status.
     */
    int SIMLOCK_STATE_UNKNOWN              = -1;

    /**
     * Status OK. Set by the server in RemoteUnlockResponse.
     */
    byte SONY_REMOTE_UNLOCK_STATUS_OK      = 0;

    /**
     * Operator specific customization,
     * server URI.
     */
    String OPERATOR_URI = "OPERATOR_URI";

    /**
     * Operator specific customization,
     * pinned CA certificate entry.
     */
    String OPERATOR_CERT_PIN_ENTRY = "OPERATOR_CERT_PIN_ENTRY";

    /**
     * Operator specific customization,
     * API key.
     */
    String OPERATOR_API_KEY = "OPERATOR_API_KEY";
}
