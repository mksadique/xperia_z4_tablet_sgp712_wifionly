/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.client;

import java.nio.ByteBuffer;

import android.content.Context;

import com.sonymobile.simlock.communication.ClientServerConstants;

/**
 * Data from the client to the server Big Endian byte order, Message format,
 * byte, Message type, short, Entire message length, byte, Type of request, i.e.
 * permanent, temporary or reconfig, byte[16], IMEI, short PKIKeyId, byte[] Blob <br>
 * <br>
 * To request an unlock of the phone, a RemoteUnlockRequest shall be created,
 * serialized with {@link #toBytes()} and sent to the server. The connection
 * with the server shall use HTTPS and the header "ApiKey" must be added, see
 * {@link 
 * com.sonymobile.simlock.api.SimlockClientAPI#getOperatorProperties(Context, String)}
 */
public class RemoteUnlockRequest implements ClientServerConstants {

    /**
     * Offset to were the blob data begins.
     */
    private static int dataOffset = 1 + 2 + 1 + IMEI_LENGTH + 2;

    /**
     * The type of unlock to request, either
     * ClientServerConstants.SONY_REMOTE_UNLOCK_PERMANENT,
     * ClientServerConstants.SONY_REMOTE_UNLOCK_TEMPORARY or
     * ClientServerConstants.SONY_REMOTE_UNLOCK_RECONFIG
     */
    private byte       unlockType = 0;

    /**
     * Current simlock blob.
     */
    private byte[]     blob       = null;

    /**
     * IMEI as a byte array.
     */
    private byte[]     imei       = null;

    /**
     * Root cert identifier. Operator specific.
     */
    private short      pkiKeyId   = 0;

    /**
     * Constructs a RemoteUnlockRequest. Use {@link #toBytes()} to get the
     * request encoded as a byte array which you can then send to the server.
     *
     * @param unlockType The type of unlock to request, either
     *            ClientServerConstants.SONY_REMOTE_UNLOCK_PERMANENT,
     *            ClientServerConstants.SONY_REMOTE_UNLOCK_TEMPORARY or
     *            ClientServerConstants.SONY_REMOTE_UNLOCK_RECONFIG
     * @param imei The phone's IMEI as a byte array.
     * @param pkiKeyId Identifier for the root cert used. Operator specific.
     * @param blob Current simlock blob.
     * @throws IllegalArgumentException On invalid unlock type, if IMEI or blob
     *             was null, if IMEI is not 16bytes long, if blob is too large.
     */
    public RemoteUnlockRequest(byte unlockType, byte[] imei, short pkiKeyId, byte[] blob) {
        if ((unlockType != SONY_REMOTE_UNLOCK_PERMANENT)
                && (unlockType != SONY_REMOTE_UNLOCK_TEMPORARY)
                && (unlockType != SONY_REMOTE_UNLOCK_RECONFIG)) {
            throw new IllegalArgumentException("Invalid unlock type");
        }
        if ((imei == null) || (blob == null)) {
            throw new IllegalArgumentException(
                    "Invalid indata (blob or imei was null)");
        }
        if (imei.length != IMEI_LENGTH) {
            throw new IllegalArgumentException("imei should be 16 bytes, not "
                    + imei.length);
        }
        if (blob.length > MAX_BLOB_LENGTH) {
            throw new IllegalArgumentException("Blob length: " + blob.length);
        }

        this.unlockType = unlockType;
        this.imei = imei;
        this.pkiKeyId = pkiKeyId;
        this.blob = blob;
    }

    @SuppressWarnings("unused")
    private RemoteUnlockRequest() {
    }

    /**
     * This method returns the unlock request as a byte array which should be
     * sent to the server and parsed by a
     * {@link com.sonymobile.simlock.communication.server.RemoteUnlockRequestParser}.
     *
     * @return Serialized RemoteUnlockRequest.
     */
    public final byte[] toBytes() {
        short dataLength = (short) (dataOffset + blob.length);
        ByteBuffer out = ByteBuffer.allocate(dataLength);

        out.put(SONY_REMOTE_UNLOCK_CLIENT_DATA_V1); // type of message
        out.putShort(dataLength); // length message
        out.put(unlockType); // type of unlock request
        out.put(imei); // IMEI
        out.putShort(pkiKeyId);
        out.put(blob); // simlock blob

        out.flip();
        byte[] b = new byte[out.remaining()];
        out.get(b, 0, b.length);
        return b;
    }
}
