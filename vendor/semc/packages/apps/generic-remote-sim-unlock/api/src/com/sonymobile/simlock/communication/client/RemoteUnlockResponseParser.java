/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.client;

import java.nio.ByteBuffer;

import com.sonymobile.simlock.communication.ClientServerConstants;

/**
 * Parser of data sent from the server to the client. Big Endian byte order,
 * Message format, byte, Message type, short, Entire message length, int, Server
 * status code, short, Length server message, byte[], Server message, byte[],
 * Server blob. <br>
 * <br>
 * After instantiation, this object can be used to extract the different
 * entities contained in the response (server status, simlock blob, server
 * message).
 */
public class RemoteUnlockResponseParser implements ClientServerConstants {

    /**
     * Offset to where the data begins.
     */
    private static int dataOffset    = (1 + 2 + 4 + 2);

    /**
     * The operator specific status code returned by the server.
     */
    private int        serverStatus  = 0;

    /**
     * The new simlock blob returned by the server.
     */
    private byte[]     blob          = null;

    /**
     * Byte encoded string containing the message from the server.
     */
    private byte[]     serverMessage = null;

    /**
     * Constructor for the RemoteUnlockParser.
     *
     * @param data The serialized data returned from the server.
     * @throws IllegalArgumentException if data is null, if data is too short,
     *             if the message type is incorrect.
     */
    public RemoteUnlockResponseParser(final byte[] data) {
        if (data == null) {
            throw new IllegalArgumentException("Invalid indata");
        }
        if (data.length < dataOffset) {
            throw new IllegalArgumentException("Response length is "
                    + data.length + " bytes");
        }

        ByteBuffer in = ByteBuffer.wrap(data);
        byte messageType = in.get(); // type of message
        if (messageType != SONY_REMOTE_UNLOCK_SERVER_DATA_V1) {
            throw new IllegalArgumentException("Expected type "
                    + SONY_REMOTE_UNLOCK_SERVER_DATA_V1 + ", got "
                    + messageType);
        }
        short dataLength = in.getShort(); // length message
        if (dataLength != data.length) {
            throw new IllegalArgumentException("Expected length: " + dataLength
                    + ", got: " + data.length);
        }

        serverStatus = in.getInt(); // status code from server
        int lengthServerMessage = in.getShort(); // length message, bytes
        serverMessage = new byte[lengthServerMessage];
        in.get(serverMessage); // optional server message
        blob = new byte[data.length - lengthServerMessage - dataOffset];
        in.get(blob); // simlock blob
    }

    // no default cntr
    @SuppressWarnings("unused")
    private RemoteUnlockResponseParser() {
    }

    /**
     * Returns the simlock blob contained in this response.
     *
     * @return simlock blob extracted from the data.
     */
    public byte[] getBlob() {
        return blob;
    }

    /**
     * Returns the server status contained in this response. The status codes
     * are operator specific.
     *
     * @return status code sent from the server.
     */
    public int getServerStatus() {
        return serverStatus;
    }

    /**
     * Returns the message from the server (set in this response). The message
     * can for example contain the string message that should be shown to the
     * user after the unlock; it is however up to the client apk how to use
     * this.
     *
     * @return message sent from the server.
     */
    public byte[] getServerMessage() {
        return serverMessage;
    }
}
