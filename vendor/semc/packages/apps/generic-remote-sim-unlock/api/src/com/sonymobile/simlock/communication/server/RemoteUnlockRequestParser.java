/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.server;

import java.nio.ByteBuffer;

import com.sonymobile.simlock.communication.ClientServerConstants;

/**
 * Parser of data sent from the client to the server. Big Endian byte order,
 * Message format, byte, Message type, short, Entire message length, byte, Type
 * of request, i.e. permanent, temporary or reconfig, byte[16], IMEI, byte[]
 * Blob. <br>
 * <br>
 * After instantiation, this object can be used to extract the different
 * entities (unlock type, simlock blob, imei, pkiKeyId) contained in a
 * RemoteUnlockRequest serialized with the getBytes method.
 */
public class RemoteUnlockRequestParser implements ClientServerConstants {

    /**
     * Offset to where the data begins.
     */
    private static int dataOffset = (1 + 2 + 1 + IMEI_LENGTH + 2);

    /**
     * The requested unlock type, either
     * ClientServerConstants.SONY_REMOTE_UNLOCK_PERMANENT,
     * ClientServerConstants.SONY_REMOTE_UNLOCK_TEMPORARY or
     * ClientServerConstants.SONY_REMOTE_UNLOCK_RECONFIG
     */
    private byte       unlockType = 0;

    /**
     * Byte encoded IMEI.
     */
    private byte[]     imei       = null;

    /**
     * Root cert identifier. Operator specific.
     */
    private short      pkiKeyId   = 0;

    /**
     * Requester's current simlock blob.
     */
    private byte[]     blob       = null;

    /**
     * Constructor for the RemoteUnlockParser.
     *
     * @param data The serialized data sent to the server.
     * @throws IllegalArgumentException if data is null, if data is too short,
     *             if the message type is incorrect.
     */
    public RemoteUnlockRequestParser(byte[] data) {
        if (data == null) {
            throw new IllegalArgumentException("Invalid indata");
        }
        if (data.length < dataOffset) {
            throw new IllegalArgumentException("Response length is "
                    + data.length + " bytes");
        }

        ByteBuffer in = ByteBuffer.wrap(data);
        byte messageType = in.get(); // type of message
        if (messageType != SONY_REMOTE_UNLOCK_CLIENT_DATA_V1) {
            throw new IllegalArgumentException("Expected type "
                    + SONY_REMOTE_UNLOCK_CLIENT_DATA_V1 + ", got "
                    + messageType);
        }
        int dataLength = in.getShort(); // length message
        if (dataLength != data.length) {
            throw new IllegalArgumentException("Expected length: " + dataLength
                    + ", got: " + data.length);
        }

        unlockType = in.get(); // type of request
        imei = new byte[IMEI_LENGTH];
        in.get(imei);
        pkiKeyId = in.getShort();
        blob = new byte[data.length - dataOffset];
        in.get(blob); // simlock blob
    }

    /**

     */

    /**
     * Returns the type of unlock requested, either
     * ClientServerConstants.SONY_REMOTE_UNLOCK_PERMANENT,
     * ClientServerConstants.SONY_REMOTE_UNLOCK_TEMPORARY or
     * ClientServerConstants.SONY_REMOTE_UNLOCK_RECONFIG
     *
     * @return type of unlock request.
     */
    public final byte getUnlockType() {
        return unlockType;
    }

    /**
     * Returns the IMEI of the phone that the request was sent from.
     *
     * @return IMEI as a byte array.
     */
    public final byte[] getImei() {
        return imei;
    }

    /**
     * Returns the identifier of the root cert with which to verify the blob.
     * The root cert is used by the SimlockToolAPI implementation.
     *
     * @return Root cert identifier.
     */
   public final short getPKIKeyId() {
       return pkiKeyId;
   }

    /**
     * Returns the simlock blob of the user who made the unlock request.
     *
     * @return user's current simlock blob as a byte array.
     */
    public final byte[] getBlob() {
        return blob;
    }
}
