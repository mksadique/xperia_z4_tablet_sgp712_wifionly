/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.server;

import java.nio.ByteBuffer;

import com.sonymobile.simlock.communication.ClientServerConstants;

/**
 * Data from the server to the client. Big Endian byte order, Message format,
 * byte, Message type, short, Entire message length, int, Server status code,
 * short, Length server message, byte[], Server message, byte[], Server blob <br>
 * <br>
 * The RemoteUnlockResponse is what the server sends to the client after
 * receiving a RemoteUnlockRequest. It contains a status code, a message from
 * the server and a new simlock blob (if the request was granted).
 */
public class RemoteUnlockResponse implements ClientServerConstants {

    /**
     * Offset to where the data begins.
     */
    private static int dataOffset = (1 + 2 + 4 + 2);

    /**
     * Operator-specific status code.
     */
    private int        statusCode = 0;

    /**
     * Bye-encoded String with a message to send to the client.
     */
    private byte[]     message    = null;

    /**
     * New simlock blob for the client to apply.
     */
    private byte[]     blob       = null;

    /**
     * Constructs a RemoteUnlockResponse with the chosen input parameters. Use
     * {@link #toBytes()} to serialize the response into a byte array which can
     * then be sent to the client.
     *
     * @param statusCode Operator specific status code to send to the client.
     * @param message Byte encoded String to send to the client.
     * @param blob New simlock blob which the client should apply.
     * @throws IllegalArgumentException If message or blob is null, if blob is
     *             too large.
     */
    public RemoteUnlockResponse(int statusCode, byte[] message, byte[] blob) {
        if ((message == null) || (blob == null)) {
            throw new IllegalArgumentException("Invalid indata");
        }
        if (blob.length > MAX_BLOB_LENGTH) {
            throw new IllegalArgumentException("Blob length: " + blob.length);
        }

        this.statusCode = statusCode;
        this.message = message;
        this.blob = blob;
    }

    @SuppressWarnings("unused")
    private RemoteUnlockResponse() {
    }

    /**
     * Serializes this RemoteUnlockResponse into a byte array which should be
     * sent back to the client and parsed by a
     * {@link com.sonymobile.simlock.communication.client.RemoteUnlockResponseParser}
     *
     * @return Serialized RemoteUnlockResponse.
     */
    public final byte[] toBytes() {
        short dataLength = (short) (dataOffset + message.length + blob.length);
        ByteBuffer out = ByteBuffer.allocate(dataLength);

        out.put(SONY_REMOTE_UNLOCK_SERVER_DATA_V1); // type of message
        out.putShort(dataLength); // length message
        out.putInt(statusCode); // server status code
        out.putShort((short) message.length); // message length
        out.put(message); // message text
        out.put(blob); // simlock blob

        out.flip();
        byte[] b = new byte[out.remaining()];
        out.get(b, 0, b.length);
        return b;
    }
}
