/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.server;

/**
 * Server side SimlockBlob update. getSignDataFormatted() main use is when an
 * HSM is involved which can only encrypt/decrypt with private key. Encrypt the
 * formatted data with the private key to create the signature. Note: Use either
 * permUnlock() or tempUnlock() depending on use case. Note: Use either
 * getSignData() or getSignDataFormatted() depending on which is appropriate.
 */
public interface SimlockToolAPI {
    /**
     * Signature check of simlock blob (after verify)
     *
     * @param pubRootCert public root certificate as a byte array.
     */
    void check(byte[] pubRootCert);

    /**
     * Final formatting of the simlock blob.
     *
     * @param signature
     * @return blob
     */
    byte[] complete(byte[] signature);

    /**
     * Public hash of the signing certificate.
     *
     * @return hash
     */
    String getPubHash();

    /**
     * Returns signdata which should be signed with private key, using SHA256
     * hash and PKCS#1 padding.
     *
     * @param pubSignCert
     * @return sign data
     */
    byte[] getSignData(byte[] pubSignCert);

    /**
     * Returns formatted signdata which should be signed with private key.
     *
     * @param pubSignCert
     * @return sign data
     */
    byte[] getSignDataFormatted(byte[] pubSignCert);

    /**
     * Modify the simlock blob by applying a permanent unlock.
     *
     * @return status
     */
    boolean permUnlock();

    /**
     * Modify the simlock blob by applying a permanent unlock.
     *
     * @param template
     * @return boolean
     */
    boolean reconfig(java.io.BufferedReader template);

    /**
     * Modify the simlock blob by applying a temporary unlock.
     *
     * @param start
     * @param duration
     * @return status
     */
    boolean tempUnlock(String start, int duration);

    /**
     * Basic info.
     *
     * @return info
     */
    @Override
    String toString();

    /**
     * Parse and verify simlock blob.
     *
     * @param imei
     * @param pubRootCert
     */
    void verify(String imei, byte[] pubRootCert);
}
