/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.test;

import java.util.Arrays;

import junit.framework.TestCase;

import com.sonymobile.simlock.communication.client.RemoteUnlockRequest;
import com.sonymobile.simlock.communication.server.RemoteUnlockRequestParser;

public class RemoteUnlockClientToServer extends TestCase {
    private byte[] imei = "1234567890123456".getBytes();
    private byte[] blob = "MyBlob".getBytes();
    private short pkiKeyId = (short)0xB316;

    public void testToBytes() {
        RemoteUnlockRequest clientData = new RemoteUnlockRequest(
                RemoteUnlockRequest.SONY_REMOTE_UNLOCK_PERMANENT, imei, pkiKeyId, blob);

        assertNotNull(clientData.toBytes());
    }

    public void testClientDataToParser() {
        RemoteUnlockRequest clientData = new RemoteUnlockRequest(
                RemoteUnlockRequest.SONY_REMOTE_UNLOCK_PERMANENT, imei, pkiKeyId, blob);
        RemoteUnlockRequestParser parser = new RemoteUnlockRequestParser(
                clientData.toBytes());

        assertTrue(parser.getUnlockType() == RemoteUnlockRequest.SONY_REMOTE_UNLOCK_PERMANENT);
        assertTrue(Arrays.equals(imei, parser.getImei()));
        assertTrue(parser.getPKIKeyId() == pkiKeyId);
        assertTrue(Arrays.equals(blob, parser.getBlob()));
    }

    public void testImeiException() {
        byte[] imei = "1".getBytes();

        try {
            new RemoteUnlockRequest(
                    RemoteUnlockRequest.SONY_REMOTE_UNLOCK_PERMANENT, imei, pkiKeyId,
                    blob);
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    public void testBlobException() {
        try {
            new RemoteUnlockRequest(
                    RemoteUnlockRequest.SONY_REMOTE_UNLOCK_PERMANENT, imei, pkiKeyId,
                    null);
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}
