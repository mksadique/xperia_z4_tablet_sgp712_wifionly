/*
 * Copyright (C) 2014 Sony Mobile Communications Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.sonymobile.simlock.communication.test;

import java.util.Arrays;

import junit.framework.TestCase;

import com.sonymobile.simlock.communication.ClientServerConstants;
import com.sonymobile.simlock.communication.client.RemoteUnlockResponseParser;
import com.sonymobile.simlock.communication.server.RemoteUnlockResponse;

public class RemoteUnlockServerToClient extends TestCase {
    private byte[] serverMessage = "Hello from server!".getBytes();
    private byte[] blob          = "MyUpdatedBlob".getBytes();

    public void testToBytes() {
        RemoteUnlockResponse serverData = new RemoteUnlockResponse(
                ClientServerConstants.SONY_REMOTE_UNLOCK_STATUS_OK,
                serverMessage, blob);
        assertNotNull(serverData.toBytes());
    }

    public void testServerDataToParser() {
        RemoteUnlockResponse serverData = new RemoteUnlockResponse(
                ClientServerConstants.SONY_REMOTE_UNLOCK_STATUS_OK,
                serverMessage, blob);
        RemoteUnlockResponseParser test = new RemoteUnlockResponseParser(
                serverData.toBytes());

        assertTrue(test.getServerStatus() == ClientServerConstants.SONY_REMOTE_UNLOCK_STATUS_OK);
        assertTrue(Arrays.equals(serverMessage, test.getServerMessage()));
        assertTrue(Arrays.equals(blob, test.getBlob()));
    }

    public void testBlobException() {
        try {
            new RemoteUnlockResponse(
                    RemoteUnlockResponse.SONY_REMOTE_UNLOCK_PERMANENT,
                    serverMessage, null);
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}
