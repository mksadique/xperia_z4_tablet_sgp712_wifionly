diff --git a/kernel/drivers/firmware/qcom/tz_log.c b/kernel/drivers/firmware/qcom/tz_log.c
index 0241cb6..7767b97 100644
--- a/kernel/drivers/firmware/qcom/tz_log.c
+++ b/kernel/drivers/firmware/qcom/tz_log.c
@@ -10,6 +10,14 @@
  * GNU General Public License for more details.
  *
  */
+/*
+ * Copyright (C) 2016 Sony Mobile Communications Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2, as
+ * published by the Free Software Foundation.
+ */
+
 #include <linux/debugfs.h>
 #include <linux/errno.h>
 #include <linux/delay.h>
@@ -18,6 +26,7 @@
 #include <linux/kernel.h>
 #include <linux/module.h>
 #include <linux/platform_device.h>
+#include <linux/proc_fs.h>
 #include <linux/slab.h>
 #include <linux/string.h>
 #include <linux/types.h>
@@ -27,7 +36,7 @@
 
 /* QSEE_LOG_BUF_SIZE = 32K */
 #define QSEE_LOG_BUF_SIZE 0x8000
-
+#define TZBSP_DIAG_MAGIC 0x747a6461
 
 /* TZ Diagnostic Area legacy version number */
 #define TZBSP_DIAG_MAJOR_VERSION_LEGACY	2
@@ -189,8 +198,6 @@ enum tzdbg_stats_type {
 	TZDBG_INTERRUPT,
 	TZDBG_VMID,
 	TZDBG_GENERAL,
-	TZDBG_LOG,
-	TZDBG_QSEE_LOG,
 	TZDBG_STATS_MAX
 };
 
@@ -203,10 +210,16 @@ struct tzdbg {
 	void __iomem *virt_iobase;
 	struct tzdbg_t *diag_buf;
 	char *disp_buf;
+	char *merge_buf;
+	uint32_t merge_buf_len;
+	uint32_t max_merge_buf_len;
 	int debug_tz[TZDBG_STATS_MAX];
 	struct tzdbg_stat stat[TZDBG_STATS_MAX];
+	int (*disp_stat[TZDBG_STATS_MAX])(void);
 };
 
+static uint32_t debug_rw_buf_size;
+
 static struct tzdbg tzdbg = {
 
 	.stat[TZDBG_BOOT].name = "boot",
@@ -214,13 +227,8 @@ static struct tzdbg tzdbg = {
 	.stat[TZDBG_INTERRUPT].name = "interrupt",
 	.stat[TZDBG_VMID].name = "vmid",
 	.stat[TZDBG_GENERAL].name = "general",
-	.stat[TZDBG_LOG].name = "log",
-	.stat[TZDBG_QSEE_LOG].name = "qsee_log",
 };
 
-static struct tzdbg_log_t *g_qsee_log;
-static uint32_t debug_rw_buf_size;
-
 /*
  * Debugfs data structure and functions
  */
@@ -380,313 +388,81 @@ static int _disp_tz_interrupt_stats(void)
 	return len;
 }
 
-static int _disp_tz_log_stats_legacy(void)
-{
-	int len = 0;
-	unsigned char *ptr;
-
-	ptr = (unsigned char *)tzdbg.diag_buf +
-					tzdbg.diag_buf->ring_off;
-	len += snprintf(tzdbg.disp_buf, (debug_rw_buf_size - 1) - len,
-							"%s\n", ptr);
 
-	tzdbg.stat[TZDBG_LOG].data = tzdbg.disp_buf;
-	return len;
-}
-
-static int _disp_log_stats(struct tzdbg_log_t *log,
-			struct tzdbg_log_pos_t *log_start, uint32_t log_len,
-			size_t count, uint32_t buf_idx)
+#define MAX_BANNER_LEN 1024
+static int merge_buffers(void)
 {
-	uint32_t wrap_start;
-	uint32_t wrap_end;
-	uint32_t wrap_cnt;
-	int max_len;
-	int len = 0;
-	int i = 0;
-
-	wrap_start = log_start->wrap;
-	wrap_end = log->log_pos.wrap;
-
-	/* Calculate difference in # of buffer wrap-arounds */
-	if (wrap_end >= wrap_start) {
-		wrap_cnt = wrap_end - wrap_start;
-	} else {
-		/* wrap counter has wrapped around, invalidate start position */
-		wrap_cnt = 2;
-	}
-
-	if (wrap_cnt > 1) {
-		/* end position has wrapped around more than once, */
-		/* current start no longer valid                   */
-		log_start->wrap = log->log_pos.wrap - 1;
-		log_start->offset = (log->log_pos.offset + 1) % log_len;
-	} else if ((wrap_cnt == 1) &&
-		(log->log_pos.offset > log_start->offset)) {
-		/* end position has overwritten start */
-		log_start->offset = (log->log_pos.offset + 1) % log_len;
-	}
-
-	while (log_start->offset == log->log_pos.offset) {
-		/*
-		 * No data in ring buffer,
-		 * so we'll hang around until something happens
-		 */
-		unsigned long t = msleep_interruptible(50);
-		if (t != 0) {
-			/* Some event woke us up, so let's quit */
-			return 0;
-		}
-
-		if (buf_idx == TZDBG_LOG)
-			memcpy_fromio((void *)tzdbg.diag_buf, tzdbg.virt_iobase,
-						debug_rw_buf_size);
-
-	}
-
-	max_len = (count > debug_rw_buf_size) ? debug_rw_buf_size : count;
-
-	/*
-	 *  Read from ring buff while there is data and space in return buff
-	 */
-	while ((log_start->offset != log->log_pos.offset) && (len < max_len)) {
-		tzdbg.disp_buf[i++] = log->log_buf[log_start->offset];
-		log_start->offset = (log_start->offset + 1) % log_len;
-		if (0 == log_start->offset)
-			++log_start->wrap;
-		++len;
-	}
-
-	/*
-	 * return buffer to caller
-	 */
-	tzdbg.stat[buf_idx].data = tzdbg.disp_buf;
-	return len;
-}
-
-static int _disp_tz_log_stats(size_t count)
-{
-	static struct tzdbg_log_pos_t log_start = {0};
-	struct tzdbg_log_t *log_ptr;
-	log_ptr = (struct tzdbg_log_t *)((unsigned char *)tzdbg.diag_buf +
-				tzdbg.diag_buf->ring_off -
-				offsetof(struct tzdbg_log_t, log_buf));
-
-	return _disp_log_stats(log_ptr, &log_start,
-				tzdbg.diag_buf->ring_len, count, TZDBG_LOG);
-}
-
-static int _disp_qsee_log_stats(size_t count)
-{
-	static struct tzdbg_log_pos_t log_start = {0};
-
-	return _disp_log_stats(g_qsee_log, &log_start,
-			QSEE_LOG_BUF_SIZE - sizeof(struct tzdbg_log_pos_t),
-			count, TZDBG_QSEE_LOG);
-}
-
-static ssize_t tzdbgfs_read(struct file *file, char __user *buf,
-	size_t count, loff_t *offp)
-{
-	int len = 0;
-	int *tz_id =  file->private_data;
+	int data_len = 0, len = 0, i;
 
 	memcpy_fromio((void *)tzdbg.diag_buf, tzdbg.virt_iobase,
 						debug_rw_buf_size);
-	switch (*tz_id) {
-	case TZDBG_BOOT:
-		len = _disp_tz_boot_stats();
-		break;
-	case TZDBG_RESET:
-		len = _disp_tz_reset_stats();
-		break;
-	case TZDBG_INTERRUPT:
-		len = _disp_tz_interrupt_stats();
-		break;
-	case TZDBG_GENERAL:
-		len = _disp_tz_general_stats();
-		break;
-	case TZDBG_VMID:
-		len = _disp_tz_vmid_stats();
-		break;
-	case TZDBG_LOG:
-		if (TZBSP_DIAG_MAJOR_VERSION_LEGACY <
-				(tzdbg.diag_buf->version >> 16)) {
-			len = _disp_tz_log_stats(count);
-			*offp = 0;
-		} else {
-			len = _disp_tz_log_stats_legacy();
+	for (i = 0; i < TZDBG_STATS_MAX; i++) {
+		if ((len + MAX_BANNER_LEN + debug_rw_buf_size) <
+				tzdbg.max_merge_buf_len) {
+			len += snprintf(tzdbg.merge_buf + len,
+				MAX_BANNER_LEN, "\n\n--------%s--------\n\n",
+				tzdbg.stat[i].name);
+			data_len = tzdbg.disp_stat[i]();
+			len += snprintf(tzdbg.merge_buf + len, data_len, "%s",
+				tzdbg.stat[(i)].data);
+			memset(tzdbg.disp_buf, 0x0, debug_rw_buf_size);
 		}
-		break;
-	case TZDBG_QSEE_LOG:
-		len = _disp_qsee_log_stats(count);
-		*offp = 0;
-		break;
-	default:
-		break;
 	}
 
-	if (len > count)
-		len = count;
-
-	return simple_read_from_buffer(buf, len, offp,
-				tzdbg.stat[(*tz_id)].data, len);
-}
-
-static int tzdbgfs_open(struct inode *inode, struct file *pfile)
-{
-	pfile->private_data = inode->i_private;
+	tzdbg.merge_buf_len = len;
+	pr_info("Length of merged buffers %d\n", len);
 	return 0;
 }
 
-const struct file_operations tzdbg_fops = {
-	.owner   = THIS_MODULE,
-	.read    = tzdbgfs_read,
-	.open    = tzdbgfs_open,
-};
-
-static struct ion_client  *g_ion_clnt;
-static struct ion_handle *g_ihandle;
-
-/*
- * Allocates log buffer from ION, registers the buffer at TZ
- */
-static void tzdbg_register_qsee_log_buf(void)
+static ssize_t tzbsp_log_read(struct file *file, char __user *buf,
+	size_t len, loff_t *offset)
 {
-	/* register log buffer scm request */
-	struct qseecom_reg_log_buf_ireq req;
-
-	/* scm response */
-	struct qseecom_command_scm_resp resp = {};
-	ion_phys_addr_t pa = 0;
-	size_t len;
-	int ret = 0;
-
-	/* Create ION msm client */
-	g_ion_clnt = msm_ion_client_create("qsee_log");
-	if (g_ion_clnt == NULL) {
-		pr_err("%s: Ion client cannot be created\n", __func__);
-		return;
-	}
-
-	g_ihandle = ion_alloc(g_ion_clnt, QSEE_LOG_BUF_SIZE,
-			4096, ION_HEAP(ION_QSECOM_HEAP_ID), 0);
-	if (IS_ERR_OR_NULL(g_ihandle)) {
-		pr_err("%s: Ion client could not retrieve the handle\n",
-			__func__);
-		goto err1;
-	}
-
-	ret = ion_phys(g_ion_clnt, g_ihandle, &pa, &len);
-	if (ret) {
-		pr_err("%s: Ion conversion to physical address failed\n",
-			__func__);
-		goto err2;
-	}
+	loff_t pos = *offset;
+	ssize_t count;
 
-	req.qsee_cmd_id = QSEOS_REGISTER_LOG_BUF_COMMAND;
-	req.phy_addr = (uint32_t)pa;
-	req.len = len;
-
-	if (!is_scm_armv8()) {
-		/*  SCM_CALL  to register the log buffer */
-		ret = scm_call(SCM_SVC_TZSCHEDULER, 1,  &req, sizeof(req),
-			&resp, sizeof(resp));
-	} else {
-		struct scm_desc desc = {0};
-		desc.args[0] = (uint32_t)pa;
-		desc.args[1] = len;
-		desc.arginfo = 0x22;
-		ret = scm_call2(SCM_QSEEOS_FNID(1, 6), &desc);
-		resp.result = desc.ret[0];
-	}
+	if (pos >= tzdbg.merge_buf_len)
+		return 0;
 
-	if (ret) {
-		pr_err("%s: scm_call to register log buffer failed\n",
-			__func__);
-		goto err2;
-	}
+	count = min(len, (size_t)(tzdbg.merge_buf_len - pos));
+	if (copy_to_user(buf, tzdbg.merge_buf + pos, count))
+		return -EFAULT;
 
-	if (resp.result != QSEOS_RESULT_SUCCESS) {
-		pr_err(
-		"%s: scm_call to register log buf failed, resp result =%d\n",
-		__func__, resp.result);
-		goto err2;
-	}
-
-	g_qsee_log =
-		(struct tzdbg_log_t *)ion_map_kernel(g_ion_clnt, g_ihandle);
-
-	if (IS_ERR(g_qsee_log)) {
-		pr_err("%s: Couldn't map ion buffer to kernel\n",
-			__func__);
-		goto err2;
-	}
+	*offset += count;
+	return count;
+}
 
-	g_qsee_log->log_pos.wrap = g_qsee_log->log_pos.offset = 0;
-	return;
 
-err2:
-	ion_free(g_ion_clnt, g_ihandle);
-	g_ihandle = NULL;
-err1:
-	ion_client_destroy(g_ion_clnt);
-	g_ion_clnt = NULL;
-}
+static const struct file_operations tzbsp_log_fops = {
+	.owner = THIS_MODULE,
+	.read = tzbsp_log_read,
+};
 
 static int  tzdbgfs_init(struct platform_device *pdev)
 {
-	int rc = 0;
-	int i;
-	struct dentry           *dent_dir;
-	struct dentry           *dent;
+	struct proc_dir_entry *entry;
 
-	dent_dir = debugfs_create_dir("tzdbg", NULL);
-	if (dent_dir == NULL) {
-		dev_err(&pdev->dev, "tzdbg debugfs_create_dir failed\n");
+	entry = proc_create_data("tzbsp_log",
+		S_IFREG | S_IRUGO, NULL, &tzbsp_log_fops, NULL);
+	if (!entry) {
+		dev_err(&pdev->dev, "Failed to create proc entry tzbsp_log\n");
 		return -ENOMEM;
 	}
 
-	for (i = 0; i < TZDBG_STATS_MAX; i++) {
-		tzdbg.debug_tz[i] = i;
-		dent = debugfs_create_file(tzdbg.stat[i].name,
-				S_IRUGO, dent_dir,
-				&tzdbg.debug_tz[i], &tzdbg_fops);
-		if (dent == NULL) {
-			dev_err(&pdev->dev, "TZ debugfs_create_file failed\n");
-			rc = -ENOMEM;
-			goto err;
-		}
-	}
 	tzdbg.disp_buf = kzalloc(debug_rw_buf_size, GFP_KERNEL);
 	if (tzdbg.disp_buf == NULL) {
 		pr_err("%s: Can't Allocate memory for tzdbg.disp_buf\n",
 			__func__);
-
-		goto err;
+		remove_proc_entry("tzbsp_log", NULL);
+		return -EINVAL;
 	}
-	platform_set_drvdata(pdev, dent_dir);
-	return 0;
-err:
-	debugfs_remove_recursive(dent_dir);
 
-	return rc;
+	return 0;
 }
 
 static void tzdbgfs_exit(struct platform_device *pdev)
 {
-	struct dentry           *dent_dir;
 
 	kzfree(tzdbg.disp_buf);
-	dent_dir = platform_get_drvdata(pdev);
-	debugfs_remove_recursive(dent_dir);
-	if (g_ion_clnt != NULL) {
-		if (!IS_ERR_OR_NULL(g_ihandle)) {
-			ion_unmap_kernel(g_ion_clnt, g_ihandle);
-			ion_free(g_ion_clnt, g_ihandle);
-		}
-	ion_client_destroy(g_ion_clnt);
-}
 }
 
 /*
@@ -695,9 +471,8 @@ static void tzdbgfs_exit(struct platform_device *pdev)
 static int tz_log_probe(struct platform_device *pdev)
 {
 	struct resource *resource;
-	void __iomem *virt_iobase;
-	phys_addr_t tzdiag_phy_iobase;
 	uint32_t *ptr = NULL;
+	uint32_t *magic = NULL;
 
 	/*
 	 * Get address that stores the physical location diagnostic data
@@ -713,35 +488,26 @@ static int tz_log_probe(struct platform_device *pdev)
 	 * Get the debug buffer size
 	 */
 	debug_rw_buf_size = resource->end - resource->start + 1;
+	printk("Tzbsp log driver initilized %u@%llx\n",
+			(unsigned int)debug_rw_buf_size, resource->start);
 
 	/*
 	 * Map address that stores the physical location diagnostic data
 	 */
-	virt_iobase = devm_ioremap_nocache(&pdev->dev, resource->start,
+	tzdbg.virt_iobase = devm_ioremap_nocache(&pdev->dev, resource->start,
 				debug_rw_buf_size);
-	if (!virt_iobase) {
+	if (!tzdbg.virt_iobase) {
 		dev_err(&pdev->dev,
 			"%s: ERROR could not ioremap: start=%pr, len=%u\n",
 			__func__, &resource->start,
 			(unsigned int)(debug_rw_buf_size));
 		return -ENXIO;
 	}
-	/*
-	 * Retrieve the address of diagnostic data
-	 */
-	tzdiag_phy_iobase = readl_relaxed(virt_iobase);
 
-	/*
-	 * Map the diagnostic information area
-	 */
-	tzdbg.virt_iobase = devm_ioremap_nocache(&pdev->dev,
-				tzdiag_phy_iobase, debug_rw_buf_size);
-
-	if (!tzdbg.virt_iobase) {
-		dev_err(&pdev->dev,
-			"%s: ERROR could not ioremap: start=%pr, len=%u\n",
-			__func__, &tzdiag_phy_iobase,
-			debug_rw_buf_size);
+	/*validate tzdiag area w.r.t magic*/
+	magic = tzdbg.virt_iobase;
+	if (*magic != TZBSP_DIAG_MAGIC) {
+		pr_err("No magic found in tzbsp diag area\n");
 		return -ENXIO;
 	}
 
@@ -753,11 +519,24 @@ static int tz_log_probe(struct platform_device *pdev)
 	}
 
 	tzdbg.diag_buf = (struct tzdbg_t *)ptr;
-
 	if (tzdbgfs_init(pdev))
 		goto err;
 
-	tzdbg_register_qsee_log_buf();
+	tzdbg.max_merge_buf_len = debug_rw_buf_size * (TZDBG_STATS_MAX + 1);
+	tzdbg.merge_buf = kzalloc(tzdbg.max_merge_buf_len, GFP_KERNEL);
+	if (tzdbg.merge_buf == NULL) {
+		pr_err("%s: Can't Allocate memory: merged_buf\n",
+			__func__);
+		goto err;
+	}
+	tzdbg.disp_stat[TZDBG_BOOT] = _disp_tz_boot_stats;
+	tzdbg.disp_stat[TZDBG_RESET] = _disp_tz_reset_stats;
+	tzdbg.disp_stat[TZDBG_INTERRUPT] = _disp_tz_interrupt_stats;
+	tzdbg.disp_stat[TZDBG_VMID] = _disp_tz_vmid_stats;
+	tzdbg.disp_stat[TZDBG_GENERAL] = _disp_tz_general_stats;
+
+	merge_buffers();
+	pr_info("probe of tzbsp log done\n");
 	return 0;
 err:
 	kfree(tzdbg.diag_buf);
@@ -768,24 +547,18 @@ err:
 static int tz_log_remove(struct platform_device *pdev)
 {
 	kzfree(tzdbg.diag_buf);
+	kzfree(tzdbg.merge_buf);
 	tzdbgfs_exit(pdev);
 
 	return 0;
 }
 
-static struct of_device_id tzlog_match[] = {
-	{	.compatible = "qcom,tz-log",
-	},
-	{}
-};
-
 static struct platform_driver tz_log_driver = {
 	.probe		= tz_log_probe,
 	.remove		= tz_log_remove,
 	.driver		= {
-		.name = "tz_log",
+		.name = "rd_tzbsp_log",
 		.owner = THIS_MODULE,
-		.of_match_table = tzlog_match,
 	},
 };
 
diff --git a/kernel/drivers/soc/qcom/board-ramdump.c b/kernel/drivers/soc/qcom/board-ramdump.c
index 29216e9..af02896 100644
--- a/kernel/drivers/soc/qcom/board-ramdump.c
+++ b/kernel/drivers/soc/qcom/board-ramdump.c
@@ -29,6 +29,7 @@
 #define HEADER_SIZE (4 * SZ_1K)
 #define KMSG_LOG_SIZE ((256 * SZ_1K) + HEADER_SIZE)
 #define AMSS_LOG_SIZE ((16 * SZ_1K) + HEADER_SIZE)
+#define TZBSP_LOG_SIZE (8 * SZ_1K)
 
 #define RDTAGS_OFFSET (RAMDUMP_MEMDESC_SIZE)
 #define KMSG_LOGS_OFFSET (RAMDUMP_MEMDESC_SIZE + RDTAGS_MEM_SIZE)
@@ -136,9 +137,10 @@ static void __init ramdump_debug_memory_init(void)
 	addr = readl_relaxed(imem_base);
 
 	if (ramdump_mode) {
-		if (addr && is_aligned(addr, SZ_1M))
+		if (addr && is_aligned(addr, SZ_1M)) {
 			debug_mem_base = addr;
-		else
+			debug_mem_size = SZ_1M;
+		} else
 			debug_mem_base = 0;
 		writel_relaxed(0, imem_base);
 	} else
@@ -206,6 +208,20 @@ static struct platform_device last_amsslog_device = {
 };
 #endif
 
+#ifdef CONFIG_MSM_TZ_LOG
+static struct resource tzbsp_log_resources[] = {
+	[0] = {
+		.name	= "tzbsp_log",
+		.flags	= IORESOURCE_MEM,
+	},
+};
+
+static struct platform_device tzbsp_log_device = {
+	.name           = "rd_tzbsp_log",
+	.id             = -1,
+};
+#endif
+
 static void __init ramdump_add_devices(void)
 {
 	if (!debug_mem_base)
@@ -255,6 +271,17 @@ static void __init ramdump_add_devices(void)
 
 	platform_device_register(&last_amsslog_device);
 #endif
+
+#ifdef CONFIG_MSM_TZ_LOG
+	tzbsp_log_resources[0].start = (debug_mem_base + debug_mem_size)
+					- TZBSP_LOG_SIZE;
+	tzbsp_log_resources[0].end = (debug_mem_base + debug_mem_size) - 1;
+	tzbsp_log_device.num_resources = ARRAY_SIZE(tzbsp_log_resources);
+	tzbsp_log_device.resource = tzbsp_log_resources;
+
+	platform_device_register(&tzbsp_log_device);
+
+#endif
 }
 
 static int __init board_ramdump_init(void)
